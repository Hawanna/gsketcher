#!/bin/bash
. gsketcher.config
R_PATH=$(pwd)

# Install gsketcher
echo "Welcome to GSketcher!"
echo "Please give me a new sketch name:"

#Sketch Installation START
read SKETCH_NAME

echo "Installing $SKETCH_NAME launcher into $MENU_PATH ..."
cat > $MENU_PATH/$SUBMENU_NAME/$SKETCH_NAME.sh << EOF
#!/bin/bash
# This file has been generated.
# Change at your own risk.
SKETCH_NAME=$SKETCH_NAME
export DISPLAY=:0; $R_PATH/$PROCESSING_PATH/$PROCESSING_VERSION/processing-java --sketch=$R_PATH/$SKETCH_PATH/$SKETCH_NAME --present
EOF

echo "Installing $SKETCH_NAME skin into $SKIN_PATH/$SUBMENU_NAME ..."
cp resources/BaseSketchMenuSkin/BaseSketch.png $SKIN_PATH/$SUBMENU_NAME/$SKETCH_NAME.png

echo "Installing $SKETCH_NAME project files..."
mkdir  sketches/$SKETCH_NAME
cp sketches/BaseSketch/BaseSketch.pde sketches/$SKETCH_NAME/$SKETCH_NAME.pde
#Sketch Installation END

echo "Done! You can edit $SKETCH_NAME here:"
echo $R_PATH/$SKETCH_PATH/$SKETCH_NAME
