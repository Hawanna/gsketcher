# GSketcher #

### What is this repository for? ###

GSketcher is a tool to setup Processing3 sketches automatically on a system with gamepad-like input to speed-up the creation of new projects, that can be executed directly from the launcher menu.
This tool is being developed with the CLockworkPi GameShell in mind. Furthermore it can install 'processing-java' to directly launch uncompiled processing code on an integrated display.

### How do I get set up? ###

To install:

1. `git clone https://bitbucket.org/Hawanna/gsketcher.git`
2. `cd gsketcher`
3. `./install.sh`

To run:

`cd gsketcher`
`./run.sh`

Sketches can be modified in the following directory:

`/home/cpi/gsketcher/sketches`

### Functionality ###

* GSketcher installs the shipped processing-java application on your GameShell.
* GSketcher creates a submenu for processing sketches in the GameShell Launcher.
* GSketcher asks for a projectname and creates all relevant files for the processing project.
* GSketcher generates all files necessary, to create a menu entry in the GameShell submenu for processing sketches to start the sketch directly on your GameShell.
