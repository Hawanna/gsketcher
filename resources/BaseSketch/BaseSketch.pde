/*
  GSketcher BaseSketch
  See https://bitbucket.org/Hawanna/gsketcher/src/master/ for more information.
*/

//initiate the scene
void setup() {
  fullScreen();
  colorMode(HSB,360,1,1,1);
  background(0,0,1,1);
}

//draw call
void draw() {
  //sample pop art
  translate(random(-100,width+100),random(-100,height+100));
  fill(frameCount%360,1,1,1);
  stroke(0,0,0,1);
  strokeWeight(4);
  rect(0,0,random(20,200),random(20,200));
}