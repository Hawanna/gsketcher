#!/bin/bash
. gsketcher.config
R_PATH=$(pwd)

# Install gsketcher
echo "Thanks for using GSketcher!"
echo "Starting installation..."

echo "Installing Processing 3 - this may take a while... "
mkdir $R_PATH/$PROCESSING_PATH
PRC=$(tar xvf resources/processing_gs.tar -C $R_PATH/$PROCESSING_PATH)

echo "Installing sketch folder..."
mkdir $R_PATH/$SKETCH_PATH

echo "Installing $SUBMENU_NAME Submenu..."
mkdir $MENU_PATH/$SUBMENU_NAME
echo "Installing $SUBMENU_NAME skin into $SKIN_PATH/$SUBMENU_NAME ..."
cp resources/$SUBMENU_NAME.png $SKIN_PATH/$SUBMENU_NAME.png

echo "Installing GameShell Submenu Skins..."
mkdir $SKIN_PATH/$SUBMENU_NAME

#Sketch Installation START
SKETCH_NAME=BaseSketch

echo "Installing $SKETCH_NAME launcher into $MENU_PATH ..."
cat > $MENU_PATH/$SUBMENU_NAME/$SKETCH_NAME.sh << EOF
#!/bin/bash
# This file has been generated.
# Change at your own risk.
SKETCH_NAME=$SKETCH_NAME
export DISPLAY=:0; $R_PATH/$PROCESSING_PATH/$PROCESSING_VERSION/processing-java --sketch=$R_PATH/$SKETCH_PATH/$SKETCH_NAME --present
EOF

echo "Installing $SKETCH_NAME skin into $SKIN_PATH/$SUBMENU_NAME ..."
cp resources/BaseSketchMenuSkin/BaseSketch.png $SKIN_PATH/$SUBMENU_NAME/$SKETCH_NAME.png

echo "Installing $SKETCH_NAME project files..."
cp -r resources/BaseSketch sketches/$SKETCH_NAME
#Sketch Installation END

echo "Installation finished. You can create new sketches by running the 'run.sh' script. Start editing your sketches via (S)FTP/terminal and a texteditor of your choice. To run the sketch, just select it via launcher."
